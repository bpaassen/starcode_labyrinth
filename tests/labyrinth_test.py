#!/usr/bin/python3
"""
Tests the labyrinth functions.
"""

# Copyright (C) 2022
# Benjamin Paaßen
# starcode

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from starcode_labyrinth import logic
from starcode_labyrinth import drawing
from starcode_labyrinth import feedback

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2022, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@dfki.de'

class TestLabyrinth(unittest.TestCase):

#    def test_drawing(self):
#        # draw all the standard levels
#        drawing.draw_levels(logic.levels)

    def test_execution(self):
        # execute a simple agent on the simplest level
        def agent(free, mem):
            return logic.GO, None
        trace = logic.execute_agent(agent, logic.levels[0])

        # test the trace
        self.assertEqual(3, len(trace))
        self.assertEqual((logic.levels[0]['start_x'], logic.levels[0]['start_y'], logic.levels[0]['start_theta'], None), trace[0])
        self.assertEqual((logic.levels[0]['start_x'] + 1, logic.levels[0]['start_y'], logic.levels[0]['start_theta'], None), trace[1])
        self.assertEqual((logic.levels[0]['start_x'] + 2, logic.levels[0]['start_y'], logic.levels[0]['start_theta'], None), trace[2])

        # verify that the agent is unsuccessful on the second
        # level by not changing position
        trace = logic.execute_agent(agent, logic.levels[1])
        self.assertTrue(len(trace) > 1)
        for t in range(len(trace)):
            self.assertEqual((logic.levels[1]['start_x'], logic.levels[1]['start_y'], logic.levels[1]['start_theta'], None), trace[t])


    def test_evaluation(self):
        # check that an agent implementing Pledge's algorithm
        # solves all the levels
        def pledge_agent(free, memory):
            theta, last_action = memory
            if free:
                if theta != 0 and last_action == logic.GO:
                    # in this case, we follow the right-hand rule.
                    # So we first look right.
                    return logic.RIGHT, (theta - 90, logic.RIGHT)
                else:
                    # otherwise, go ahead
                    return logic.GO, (theta, logic.GO)
            else:
                if theta != 0 and last_action == logic.GO:
                    # in this case, we follow the right-hand rule.
                    # So we first look right
                    return logic.RIGHT, (theta - 90, logic.RIGHT)
                else:
                    # otherwise, go left
                    return logic.LEFT, (theta + 90, logic.LEFT)

        feedback.evaluate_agent(pledge_agent, start_memory = (0, logic.GO))

if __name__ == '__main__':
    unittest.main()
