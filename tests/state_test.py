#!/usr/bin/python3
"""
Tests the state functions.
"""

# Copyright (C) 2022
# Benjamin Paaßen
# starcode

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from starcode_labyrinth import state

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2022, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@dfki.de'

class TestState(unittest.TestCase):

    def test_level0(self):
        # solve the first level
        game_state = state.State(0)
        self.assertTrue(game_state.is_free())
        self.assertFalse(game_state.is_goal())
        game_state.turn_left()
        self.assertFalse(game_state.is_free())
        self.assertFalse(game_state.is_goal())
        game_state.turn_right()
        for i in range(6):
            self.assertTrue(game_state.is_free())
            self.assertFalse(game_state.is_goal())
            game_state.move()
        self.assertFalse(game_state.is_free())
        self.assertTrue(game_state.is_goal())
#        game_state.draw_trace()

    def test_level6(self):
        # solve the last level
        game_state = state.State(6)
        turns = ['right', 'right', 'left', 'right', 'left', 'left', 'left', 'right', 'right', 'right', 'left', 'right', 'left', 'left', 'right']
        for i in range(len(turns)):
            while game_state.is_free():
                game_state.move()
            if turns[i] == 'right':
                game_state.turn_right()
            else:
                game_state.turn_left()
        game_state.move()
        self.assertTrue(game_state.is_goal())
#        game_state.draw_trace()

    def test_level8(self):
        # solve the last level
        game_state = state.State(8)

        schritte = 1
        while game_state.is_free():

            for schritt in range(schritte):
                game_state.move()
            game_state.turn_right()
            if not game_state.is_free():
                game_state.turn_left()
                game_state.turn_left()
            schritte += 1
        game_state.draw_trace()
        self.assertTrue(game_state.is_goal())
if __name__ == '__main__':
    unittest.main()
